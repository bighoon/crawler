#encoding:u8

import CoreCrawler
import glob
import urllib2
import json
import requests
import random
import string
import os 
import re
import sys
import urllib
from time import sleep
import csv
import datetime, time
import urllib2,cookielib
import urllib as ul
import urllib
import mechanize
from BeautifulSoup import BeautifulSoup
from BeautifulSoup import NavigableString
from BeautifulSoup import Tag
from dateutil.relativedelta import relativedelta
try:
    from urllib.parse import urlparse
except ImportError:
     from urlparse import urlparse
from time import localtime, strftime
import httplib
import traceback
import socket
import psycopg2
from socket import error as SocketError
import errno
import shutil
from datetime import date
import collections

class Crawler:
    class Public:
        class Dart:
            def get_guidCodeList(self, fe):
                cutil = CoreCrawler.__CrawlerUtil__()
            
                htmlGuideCodeList = []
            
                searchGuideCurl = '''curl 'http://dart.fss.or.kr/dsap001/guide.do' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.220 Whale/1.3.53.4 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' -H 'Cache-Control: max-age=0' -H 'Cookie: JSESSIONID=dTaay1DzxvVshMNmx1vGzhjBI1TzQcsHXrSaA0TvEpG9MZQo0uL2vFaEBjNTx9Pp.dart2_servlet_engine1' -H 'Connection: keep-alive' --compressed'''
                htmlGuideOrigin = os.popen(searchGuideCurl).read()
                htmlGuideOrigin = cutil.unicodeDecoder(htmlGuideOrigin)
                try:
                    htmlGuideOrigin = htmlGuideOrigin.split('<caption>상세 유형 목록</caption>')[1].split('</table>')[0] # 공시정보 코드 부분 추출
                    htmlGuideOrigin = htmlGuideOrigin.split('<tbody>')[1].split('</tbody>')[0]
                    htmlGuideSplitTr = htmlGuideOrigin.split('<tr>')
                    for htmlGuideOne in htmlGuideSplitTr :
                        if '<tr class="even">' in htmlGuideOne :
                            htmlGuideSplitTre = htmlGuideOne.split('<tr class="even">')
                            for htmlGuideOne in htmlGuideSplitTre :
                                htmlGuideCodeCom = re.compile('<td class=\"cen_txt\">([A-Z]\d{3})</td>')
                                htmlGuideCodeSearch = htmlGuideCodeCom.search(htmlGuideOne)
                                if htmlGuideCodeSearch :
                                    htmlGuideCode = htmlGuideCodeSearch.group(1)
                                    htmlGuideCodeList.append(htmlGuideCode)
                        else :
                            htmlGuideCodeCom = re.compile('<td class=\"cen_txt\">([A-Z]\d{3})</td>')
                            htmlGuideCodeSearch = htmlGuideCodeCom.search(htmlGuideOne)
                            if htmlGuideCodeSearch :
                                htmlGuideCode = htmlGuideCodeSearch.group(1)
                                htmlGuideCodeList.append(htmlGuideCode)
                except IndexError:
                    cutil.commonLog(fe, traceback.format_exc())

                return htmlGuideCodeList

            def get_companyCodeList(self, fe):
                cutil = CoreCrawler.__CrawlerUtil__()
                htmlCompanyCodeList = []
                companyTypeList = ['P', 'A', 'N']
                for companyType in companyTypeList :
                    flag = True
                    page_num_c = 1
                    while(flag) :
                        searchCompanyCurl = '''curl 'http://dart.fss.or.kr/dsae001/search.ax' -H 'Cookie: JSESSIONID=aXzs84nBZwwVbj3yQnosufrckECZ9f30XvlOKQxKXNhSaRMU0X9fFAgJYaecdLM1.dart2_servlet_engine1' -H 'Origin: http://dart.fss.or.kr' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7' -H 'User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.220 Whale/1.3.53.4 Safari/537.36' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Accept: text/html, */*; q=0.01' -H 'Referer: http://dart.fss.or.kr/dsae001/main.do' -H 'X-Requested-With: XMLHttpRequest' -H 'Connection: keep-alive' --data 'startDate=&endDate=&currentPage=''' + str(page_num_c) + '''&maxResults=45&maxLinks=10&sort=&series=&selectKey=&searchIndex=&textCrpCik=&autoSearch=true&textCrpNm=&typesOfBusiness=all&corporationType=''' + companyType + '''' --compressed'''
                        htmlCompanyOrigin = os.popen(searchCompanyCurl).read()
                        htmlCompanyOrigin = cutil.unicodeDecoder(htmlCompanyOrigin)
                        htmlCompanyOrigin = cutil.html_replace(htmlCompanyOrigin)
                        if '일치하는 회사명이 없습니다.' in htmlCompanyOrigin:
                           flag = False
                        try:
                            htmlCompanyOrigin = htmlCompanyOrigin.split('<caption>기업개황 목록</caption>')[1].split('</table>')[0]
                            htmlCompanyOrigin = htmlCompanyOrigin.split('<tbody>')[1].split('</tbody>')[0]
                            htmlCompanySplitTd = htmlCompanyOrigin.split('<td>')

                            for htmlCompanyOne in htmlCompanySplitTd :
                                htmlCompanyOne = ' '.join(htmlCompanyOne.split())
                                htmlCompanyCodeCom = re.compile('<td align="center">\s([^\.]+)\s<\/td>')
                                htmlCompanyCodeSearch = htmlCompanyCodeCom.search(htmlCompanyOne)
                                if htmlCompanyCodeSearch :
                                    companyCode = htmlCompanyCodeSearch.group(1)
                                    if str(companyCode) == 0 :
                                        companyCode = '#NUUL#'
                                    companyName = htmlCompanyOne.split('name=\'hiddenCikNM\' value=\'')[1].split('\'>')[0]
                                    htmlCompanyCodeNm = companyCode + ';;;' + companyName
                                    htmlCompanyCodeList.append(htmlCompanyCodeNm)
                        except IndexError:
                            cutil.commonLog(fe, traceback.format_exc())
            
                        page_num_c += 1
                return htmlCompanyCodeList
           
            def main(self,absPath, dataPath, logPath, targetDate):
                cutil = CoreCrawler.__CrawlerUtil__()
                if not os.path.exists(dataPath):
                    try:
                        os.makedirs(dataPath)
                    except OSError:
                        if not os.path.isdir(dataPath):
                            raise
                if not os.path.exists(logPath):
                    try:
                        os.makedirs(logPath)
                    except OSError:
                        if not os.path.isdir(logPath):
                            raise
            
                next_page = ''
                baseUrl = 'http://dart.fss.or.kr/api/search.xml?auth=c921c3497787da73fdd27494eb9493dfe8054c8c'
                urlDate = '&start_dt='
                urlBsnTp = '&bsn_tp='
                urlCrpCd = '&crp_cd='
            
                html0_t = ''
                html0_f = ''
                html_t = ''
                html_f = ''
                now = time.localtime()
                nowDate = "%04d%02d%02d" % (now.tm_year, now.tm_mon, now.tm_mday)
                guideCodeList = []
                companyCodeNmList = []
                resList = []
                with open(logPath + os.sep + targetDate + '.ERR_DART_TEMP' , 'w') as fe:
                    with open(logPath + os.sep + targetDate + '.WORKING_DART_TEMP', 'w') as few:
                        with open(dataPath + os.sep + targetDate + '.DAILY_DART', 'w') as fw:
                            workingProcess = 'start crawling... '
                            workingTime = str(datetime.datetime.now())
                            working = '%s\t%s' % (workingProcess, workingTime)
                            print >> few, working
                            guideCodeList = self.get_guidCodeList(fe)
                            workingProcess = 'get guidCode... '
                            workingTime = str(datetime.datetime.now())
                            working = '%s\t%s' % (workingProcess, workingTime)
                            print >> few, working
                            companyCodeNmList = self.get_companyCodeList(fe)
                            workingProcess = 'get companyCode... '
                            workingTime = str(datetime.datetime.now())
                            working = '%s\t%s' % (workingProcess, workingTime)
                            print >> few, working
                            companyCnt = 0
                            for companyCodeNm in companyCodeNmList :
                                point = True
                                try:
                                    crpCd = companyCodeNm.split(';;;')[0]
                                    crpNm = companyCodeNm.split(';;;')[1]
                                except IndexError:
                                    cutil.commonLog(fe, traceback.format_exc())
                                companyCnt+=1
                                searchCompanyCntF = "single : " + str(companyCnt) + " total : " + str(len(companyCodeNmList))
                                for guideCode in guideCodeList :
                                    try :
                                        bsnTp = guideCode
                                        findRcpNoCurl = '''curl 'http://dart.fss.or.kr/dsab002/search.ax' -H 'Cookie: JSESSIONID=lYLeEwFp1lKwaFtplxcFU140n6nCrGYlX8oKE4xKhGoR6qeb0NcuL1RAJygYfmT5.dart1_servlet_engine2' -H 'Origin: http://dart.fss.or.kr' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.220 Whale/1.3.53.4 Safari/537.36' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Accept: text/html, */*; q=0.01' -H 'Referer: http://dart.fss.or.kr/dsab002/main.do' -H 'X-Requested-With: XMLHttpRequest' -H 'Connection: keep-alive' --data 'currentPage=1&maxResults=15&maxLinks=10&sort=date&series=desc&textCrpCik=&reportNamePopYn=&textCrpNm=''' + crpNm  + '''&textPresenterNm=&startDate='''+ targetDate  +'''&endDate=''' + targetDate  +'''&finalReport=recent&typesOfBusiness=all&corporationType=all&closingAccountsMonth=all&reportName=&publicType=''' + bsnTp + '''' --compressed'''
                                        html = os.popen(findRcpNoCurl).read()
                                        sleep(0.06)
                                        html = cutil.unicodeDecoder(html)
                                        html = cutil.html_replace(html)
                                        try:
                                            reportListOrigin = html.split('<caption>공시서류검색 목록</caption>')[1].split('<p class="des">')[0]
                                        except IndexError:
                                            cutil.commonLog(fe, traceback.format_exc())
                                        reportListByTr = reportListOrigin.split('<tr>')
                                        reportCnt = 0
                                        for report in reportListByTr :
                                            reportCnt += 1
                                            report = report.replace('\r\n', ' ').replace('\n', ' ').replace('\r', ' ').replace('\t', ' ')
                                            if '<td class="cen_txt">' in report :
                                                reportCom = re.compile('<a\shref="([^\"]+)" id=')
                                                reportSearch = reportCom.search(report)
                                                if reportSearch :
                                                    reportLink = reportSearch.group(1)
                                                    reportDateCom = re.compile('<td class="cen_txt">(\d{4}.\d{2}.\d{2})<\/td>')
                                                    reportDateSearch = reportDateCom.search(report)
                                                    rcpDt = reportDateSearch.group(1)
                                                    doc_uid = findRcpNoCurl
                                                    doc_date = targetDate
                                                    category_code = '0820510000000000'
                                                    content_ext_size = '#NULL#'
                                                    press = '#NULL#'
                                                    site = 'DART.PUBLIC'
                                                    section = '공시'
                                                    user_id = crpNm
                                                    title = crpCd
                                                    try:
                                                        content = '%s@@%s@@%s@@%s@@%s' % (rcpDt, crpCd, crpNm, bsnTp, reportLink)
                                                        res = '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s' % (doc_uid, doc_date, category_code, content_ext_size, press, site, section, user_id, title, content)
                                                        print >> fw, res
                                                        workingProcess = 'crawling... '
                                                        workingTime = str(datetime.datetime.now())
                                                        working = '%s\t%s' % (workingProcess, workingTime)
                                                        print >> few, working
                                                    except UnicodeEncodeError:
                                                        cutil.commonLog(fe, traceback.format_exc())

                                    except IndexError as e:
                                        cutil.commonLog(fe, traceback.format_exc())
                                        continue
                                    except ValueError as e:
                                        cutil.commonLog(fe, traceback.format_exc())
                                        continue
                            workingProcess = 'end crawling... '
                            workingTime = str(datetime.datetime.now())
                            working = '%s\t%s' % (workingProcess, workingTime)
                            print >> few, working